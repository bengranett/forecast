# README #


### What is this repository for? ###

This code performs a simple Fisher calculation to forecast the errors on redshift-space distortion parameters for galaxy surveys.

### Dependencies ###

* [Cosmopy](http://www.ifa.hawaii.edu/cosmopy/)
* [Sympy](http://www.sympy.org) is used to evaluate derivatives.
* numpy