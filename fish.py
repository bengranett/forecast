import os
import numpy as N

import theory
import theory.pt as Pt
import theory.frw as FRW

import theorycl
import ISWCalc
import bins

import sympy


class Fish:
    fudge = (2*N.pi)**3

    fishparams = {
        'pk_kmin': 0.10,
        'pk_kmax': 0.41,
        'pk_deltak': 0.05,

        'pk_mubins': 1000,

        'cl_lmin': 50,
        'cl_lmax': 1000,
        'cl_deltal': 50,

        'bias': 1.4,
        'sigmav': 4.0,
        'growthf': 0.7,

        'area_phot': 130.,
        'area_spec': 24.,

        'zmean': 0.7,
        'zsig': 0.25,
        'zmin': 0.1,
        'zmax': 2.0,
        'zstep': 0.01,

        'spec_zerror': 1.4,

        'ngal_phot': 1e5,
        'ngal_spec': 3e4,

        'cambpath' : '~/camb'
    }


    Pfid = None

    fullsky = 4*N.pi*(180/N.pi)**2  # sqr deg on sky


    def __init__(self, **params):
        """ """
        for k in params:
            if self.fishparams.has_key(k):
                self.fishparams[k] = params[k]
                print "> set %s: %s"%(k,str(params[k]))
            else:
                print "!!! Warning! unknown parameter: %s"%k

        self.Distance = FRW.Distance()
        self.h = self.Distance.h

        p = self.fishparams

        # initialize k and ell binning
        self.k = N.arange(p['pk_kmin'],p['pk_kmax'],p['pk_deltak'])
        self.mu = N.linspace(-1,1,p['pk_mubins'])
        self.ell = N.arange(p['cl_lmin'],p['cl_lmax'],p['cl_deltal'])

        print ">   k range",self.k.min(),self.k.max(),p['pk_deltak']
        print "> ell range",self.ell.min(),self.ell.max(),p['cl_deltal']

        # set up redshift distribution
        self.zz = N.arange(p['zmin'],p['zmax'],p['zstep'])
        self.nz = N.exp(-(self.zz-p['zmean'])**2/2./p['zsig']**2)
        self.nz /= N.sum(self.nz)
        self.dndz = (self.zz, self.nz)

        # survey geometries
        self.fsky = p['area_phot']/self.fullsky

        # compute power spectrum
        self.computePk()

        # number densities
        self.nbar_phot = p['ngal_phot']/(4*N.pi*self.fsky)
        self.effvolume()

        # Angular power spectrum
        self.computeCl()
        



    def pmodel(self,b,f,sigv,mu,k):
        """ """
        zerr = self.fishparams['spec_zerror']
        return (b+f*mu**2)**2*N.exp(-k**2*mu**2*(sigv**2+zerr**2))


    def effvolume(self):
        """ Effective volume defined by Tegmark 97 (astro-ph/9706198)"""
        p = self.fishparams

        fsky = p['area_spec']/self.fullsky

        r = self.Distance.rc(self.zz) * self.h
        r_m = (r[1:]+r[:-1])/2.
        dr = r[1:]-r[:-1]
        dV = 4./3*N.pi*(r[1:]**3 - r[:-1]**3) * fsky

        nz_m = (self.nz[1:]+self.nz[:-1])/2.

        ngal = nz_m*p['ngal_spec'] / N.sum(nz_m)
        nbar = ngal/dV
        print "> max nbar",nbar.max()


        k = self.k
        pk = self.Pfid
        Veff = N.zeros((len(k),len(self.mu)))
        for i in range(len(k)):
            for j in range(len(self.mu)):

                P = self.pmodel(p['bias'],p['growthf'],p['sigmav'],self.mu[j],k[i]) * pk[i]

                Veff[i,j] = N.sum(fsky*4*N.pi*r_m**2*dr * (nbar*1./(1+nbar*P))**2)

                
        self.Veff = Veff

        return self.Veff

    def computePk(self):
        """ """
        if self.Pfid is not None:
            return self.Pfid

        camb = Pt.Camb(cambPath=os.path.expanduser(self.fishparams['cambpath']), 
                       transfer_redshift=[self.fishparams['zmean']],
                       do_nonlinear=1)
        camb.run()

        self.cambpk = (camb.k,camb.pk)

        self.Pfid = N.exp(N.interp(N.log(self.k), N.log(camb.k),N.log(camb.pk)))
        
        return self.Pfid


    def getLeg(self, L, mu):
        if L==0:
            return 1.0
        elif L==2:
            return (3*mu**2-1)/2.
        else:
            print "not implemented!"
            exit(1)


    def PkCov(self, L1, L2):
        """ """
        p = self.fishparams
        dmu = self.mu[1]-self.mu[0]
        dk = p['pk_deltak']

        # number of modes on a spherical shell
        Vn = 4*N.pi * self.k**2 * dk

        leg1 = self.getLeg(L1, self.mu)
        leg2 = self.getLeg(L2, self.mu)

        norm = (2*L1+1)*(2*L2+1)/2.

        C = 2./Vn * norm * N.sum(leg1*leg2/self.Veff,axis=1) * dmu

        return C

    def fish_multipoles(self,deriv1,deriv2,multipoles):
        """ """
        print "> running",multipoles
        p = self.fishparams
        dmu = self.mu[1]-self.mu[0]
        F = 0.
        for L1 in multipoles:
            for L2 in multipoles:
                if L1>L2: continue
                if L1!=L2: continue

                leg1 = self.getLeg(L1, self.mu)
                leg2 = self.getLeg(L2, self.mu)

                norm1 = (2*L1+1)/2.
                norm2 = (2*L2+1)/2.

                a = norm1*N.sum(deriv1*leg1,axis=1)*dmu
                b = norm2*N.sum(deriv2*leg2,axis=1)*dmu
                C = self.PkCov(L1,L2)
                
                f = N.sum(a*b/C)/self.fudge
                print "F",L1,L2,f
                F += f
        return F

    def gofish(self, multipoles=(0,2)):
        """ 
        P(k,mu) = b**2*(1+beta*mu**2)**2 exp(-(k*mu*sig)**2)

        P_0 = int_-1^1 dmu P(k,mu)
        P_2 = int_-1^1 dmu P(k,mu) (3mu^2 - 1)/2

        """
        p = self.fishparams

        b,f,k,s,mu = sympy.symbols('b f k s mu')
        params = [b,f,s]

        fid = [(b,p['bias']),
               (f,p['growthf']),
               (s,p['sigmav'])]

        zerr = p['spec_zerror']
        P = (b+f*mu**2)**2 * sympy.exp(-k**2*mu**2*(s**2+zerr**2))

        derivs = []

        for i,x in enumerate(params):
            d = sympy.diff(P,x)
            #print "Derivative",fid[i][0],sympy.simplify(d)
            d = d.subs(fid)
            dd = N.zeros((len(self.k),len(self.mu)))
            lamfunc = sympy.lambdify((k,mu),d,"numpy")
            for j in range(len(self.k)):
                dd[j,:] = lamfunc(self.k[j], self.mu)       
            derivs.append(N.transpose(dd.T*self.Pfid))



        Fmat = N.zeros((len(derivs),len(derivs)))
        for i in range(len(derivs)):
            for j in range(len(derivs)):
                if j<i: continue
                Fmat[i,j] = self.fish_multipoles(derivs[i],derivs[j], multipoles)
                if i!=j: Fmat[j,i] = Fmat[i,j]

        return Fmat

    def computeCl(self):
        """ """
        C = ISWCalc.ISWCalculator(self.dndz,kpk=self.cambpk, runcamb=False)
        cl = C.GalaxyCl_flat(self.ell)
        self.Clfid = cl

    def Clcov(self):
        """ """
        delta_ell = self.fishparams['cl_deltal']
        return 2./((2.*self.ell+1)*self.fsky*delta_ell) * (self.Clfid+1./self.nbar_phot)**2

    def Clfish(self):
        """ """
        fb = 2*self.fishparams['bias']*self.Clfid
        err = self.Clcov()
        fish = N.sum(fb**2/err)
        return fish



